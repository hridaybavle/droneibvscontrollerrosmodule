#include "DroneIBVSControllerROSModule.h"


DroneIBVSControllerROSModule::DroneIBVSControllerROSModule():
    DroneModule(droneModule::active, 1.0/MULTIROTOR_IBVSCONTROLLER_TS),
    MyDroneIBVSController()
 {
    std::cout << "DroneIBVSControllerROSMosdule(..), enter and exit " << std::endl;

    return;
}

DroneIBVSControllerROSModule::~DroneIBVSControllerROSModule()
{
    close();
    return;
}

void DroneIBVSControllerROSModule::init()
{
    MyDroneIBVSController.init(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+configFile);
    return;
}

void DroneIBVSControllerROSModule::close()
{
    if(!MyDroneIBVSController.close())
        return;
    return;
}

bool DroneIBVSControllerROSModule::resetValues()
{
    if(!DroneModule::resetValues())
        return false;

    if(!MyDroneIBVSController.resetValues())
        return false;

    return true;
}

bool DroneIBVSControllerROSModule::startVal()
{

    if ( !isStarted() )
    {
        resetValues();
        setNavCommandToZero();
        MyDroneIBVSController.start();
    }

    return DroneModule::startVal();

}

bool DroneIBVSControllerROSModule::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    setNavCommandToZero();
    if(!MyDroneIBVSController.stopValues());
       return false;

     return true;
}

bool DroneIBVSControllerROSModule::run()
{

    MyDroneIBVSController.setTelemetryAttitude_rad( droneYaw, dronePitch, droneRoll );
    MyDroneIBVSController.setTargetIsOnFrame(tracker_object_on_frame);
    MyDroneIBVSController.boundingBox2ImageFeatures(bb_x, bb_y, bb_width, bb_height, fxs, fys, fss, fDs, tracker_object_on_frame);
    MyDroneIBVSController.setImFeatMeasurements(fxs, fys, fss, fDs, bb_width, bb_height);


    if (DroneModule::run())
    {
    if (MyDroneIBVSController.run())
    {
        DroneNavCommands = MyDroneIBVSController.getNavCommand();
        setNavCommand(DroneNavCommands.pitch, DroneNavCommands.roll, DroneNavCommands.dyaw, DroneNavCommands.dz);
    }
    }

    MyDroneIBVSController.getImFeatReferences(fxci, fyci, fsci, fDci);
    dronePublishImageFeatures();
    MyDroneIBVSController.getImFeatFeedback2PIDs(fxs_4DyC, fxs_4DYC, fys_4DzC, fDs_4DxC);
    dronePublishImageFeaturesFeedback();

}

void DroneIBVSControllerROSModule::readParameters()
{
    //Config file
    ros::param::get("~config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="ibvs_controller_config.xml";
    }

    //Subscriber names
    ros::param::get("~rotation_angles_topic_name", rotation_angles_topic_name);
    if ( rotation_angles_topic_name.length() == 0)
    {
        rotation_angles_topic_name="rotation_angles";
    }
    ros::param::get("~tracking_object_topic_name", tracking_object_topic_name);
    if ( tracking_object_topic_name.length() == 0)
    {
        tracking_object_topic_name="tracking_object";
    }
    ros::param::get("~is_object_on_frame_topic_name", is_object_on_frame_topic_name);
    if ( is_object_on_frame_topic_name.length() == 0)
    {
        is_object_on_frame_topic_name="is_object_on_frame";
    }
    ros::param::get("~get_bounding_box_topic_name", get_bounding_box_topic_name);
    if( get_bounding_box_topic_name.length() == 0)
    {
        get_bounding_box_topic_name="get_bounding_box";
    }

    //Publisher names
    ros::param::get("~command_pitch_roll_topic_name", command_pitch_roll_topic_name);
    if( command_pitch_roll_topic_name.length() == 0)
    {
        command_pitch_roll_topic_name="command/pitch_roll";
    }
    ros::param::get("~command_dAltitude_topic_name", command_dAltitude_topic_name);
    if( command_dAltitude_topic_name.length() == 0)
    {
        command_dAltitude_topic_name="command/dAltitude";
    }
    ros::param::get("~command_dYaw_topic_name", command_dYaw_topic_name);
    if( command_dYaw_topic_name.length() == 0)
    {
        command_dYaw_topic_name="command/dYaw";
    }
    ros::param::get("~controller_image_features_topic_name", controller_image_features_topic_name);
    if( controller_image_features_topic_name.length() == 0)
    {
        controller_image_features_topic_name="controller_image_features";
    }
    ros::param::get("~controller_image_features_feedback_topic_name", controller_image_features_feedback_topic_name);
    if( controller_image_features_feedback_topic_name.length() == 0)
    {
        controller_image_features_feedback_topic_name="controller_image_features_feedback";
    }

    return;
}

void DroneIBVSControllerROSModule::open(ros::NodeHandle &nIn, std::string ModuleName)
{

    DroneModule::open(nIn,ModuleName);


    readParameters();
    init();

    drone_rotation_angle_sub    = n.subscribe(rotation_angles_topic_name,1,&DroneIBVSControllerROSModule::droneRotationAnglesCallback, this);
    tracking_object_sub         = n.subscribe(tracking_object_topic_name,1,&DroneIBVSControllerROSModule::trackingObjectCallback, this);
    is_object_on_frame_sub      = n.subscribe(is_object_on_frame_topic_name,1,&DroneIBVSControllerROSModule::isObjectOnFrameCallback, this);
    get_bounding_box_sub        = n.subscribe(get_bounding_box_topic_name,1,&DroneIBVSControllerROSModule::getBoundingBoxCallback, this);

    drone_command_pitch_roll_publisher       = n.advertise<droneMsgsROS::dronePitchRollCmd>(command_pitch_roll_topic_name,1,true);
    drone_command_daltitude_publisher        = n.advertise<droneMsgsROS::droneDAltitudeCmd>(command_dAltitude_topic_name,1,true);
    drone_command_dyaw_publisher             = n.advertise<droneMsgsROS::droneDYawCmd>(command_dYaw_topic_name,1,true);
    drone_image_features_publisher           = n.advertise<droneMsgsROS::imageFeaturesIBVS>(controller_image_features_topic_name,1,true);
    drone_image_features_feedback_publisher  = n.advertise<droneMsgsROS::imageFeaturesFeedbackIBVS>(controller_image_features_feedback_topic_name,1,true);

    droneModuleOpened = true;

    return;

}


void DroneIBVSControllerROSModule::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped &msg)
{
     dronePitch =  msg.vector.y;
     droneRoll  =  msg.vector.x;
     droneYaw   =  msg.vector.z;

     dronePitch =  dronePitch*M_PI/180.0;
     droneRoll  =  droneRoll*M_PI/180.0;
     droneYaw   =  droneYaw*M_PI/180.0;
     return;
}


void DroneIBVSControllerROSModule::trackingObjectCallback(const std_msgs::Bool &msg)
{
    tracker_is_tracking = msg.data;
    return;
}

void DroneIBVSControllerROSModule::isObjectOnFrameCallback(const std_msgs::Bool &msg)
{
    tracker_object_on_frame = msg.data;
    return;
}

void DroneIBVSControllerROSModule::getBoundingBoxCallback(const droneMsgsROS::BoundingBox &msg)
{
    bb_x = msg.x;
    bb_y = msg.y;
    bb_width = msg.width;
    bb_height = msg.height;
    bb_confidence = msg.confidence;
    return;
}

void DroneIBVSControllerROSModule::dronePublishImageFeatures()
{
    droneMsgsROS::imageFeaturesIBVS image_features;
    image_features.fx = fxci;
    image_features.fy = fyci;
    image_features.fs = fsci;
    image_features.fD = fDci;
    drone_image_features_publisher.publish(image_features);
    return;
}

void DroneIBVSControllerROSModule::dronePublishImageFeaturesFeedback()
{
    droneMsgsROS::imageFeaturesFeedbackIBVS image_features_feedback;
    image_features_feedback.Dy = fxs_4DyC;
    image_features_feedback.DY = fxs_4DYC;
    image_features_feedback.Dz = fys_4DzC;
    image_features_feedback.Dx = fDs_4DxC;
    drone_image_features_feedback_publisher.publish(image_features_feedback);
    return;
}

void DroneIBVSControllerROSModule::setNavCommandToZero(void) {
    setNavCommand(0.0,0.0,0.0,0.0);
    return;
}

void DroneIBVSControllerROSModule::setNavCommand(float pitch, float roll, float dyaw, float dz)
{
    pitchrollcmdsmsg.pitchCmd       = pitch;
    pitchrollcmdsmsg.rollCmd        = roll;
    dyawcmdmsg.dYawCmd              = dyaw;
    daltitudecmdsmsg.dAltitudeCmd   = dz;

    dronePublishIBVSControllerCmds();
    return;
}

int DroneIBVSControllerROSModule::dronePublishIBVSControllerCmds()
{
    if(droneModuleOpened==false)
        return 0;

    drone_command_pitch_roll_publisher.publish(pitchrollcmdsmsg);
    drone_command_dyaw_publisher.publish(dyawcmdmsg);
    drone_command_daltitude_publisher.publish(daltitudecmdsmsg);

    return 1;

}

