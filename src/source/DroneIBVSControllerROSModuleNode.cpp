#include "ros/ros.h"
#include "DroneIBVSControllerROSModule.h"
#include "nodes_definition.h"


int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, MODULE_NAME_IBVS_CONTROLLER);
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << MODULE_NAME_IBVS_CONTROLLER << std::endl;

    DroneIBVSControllerROSModule MyIBVSControllerROSModule;
    MyIBVSControllerROSModule.open(n, MODULE_NAME_IBVS_CONTROLLER);

    try
    {
        while(ros::ok())
        {
            ros::spinOnce();
            if(MyIBVSControllerROSModule.run())
            {

            }
            MyIBVSControllerROSModule.sleep();
        }
        return 1;
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

}
